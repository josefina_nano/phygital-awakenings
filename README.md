**Phygital Awakenings**

**_Initial idea/Concept of the Project_**

Using technology as a tool to augment and amplify a human vital sign.   

**_Purpose_**

Share people's heartbeat. Increase and amplify what is limited in the virtual and physical world. 

**_Desired Impact_**

Co-creating interfaces to give meaning to a collective by creating an impact through the amplification and increase of a human sense.

**_System Diagram_**

<img src= "Assets/01systemdiagram.png">

**_Design Elements & BOM (Build of Materials)_**

<img src= "Assets/02elements.png">

**_Materials and Fabrication Processes_**
1. AD8232 ECG Monitoring Sensor 
2. Power MB V2
3. Earphones
4. Speaker

- Step 01: hacking old earphones, cut them in order to use only the cables and the plug. 
- Step 02: sold the earphone cables to the ECG sensor board / Output with cables: left (green color) & right (red color) - Ground with cable: Ground (copper color). 
- Step 03: attach with cables the ECG sensor board to the Power MB V2 / 3.3 V connect to 3.3 - GND connect to GND. 
- Step 04: replicate the previous steps 4 times. 

*Total amount of devices: 5 

**_The coding Logic_**

*analog - no coding required.

**_End Artifacts_**

<img src= "Assets/05collagepics.jpg">
<img src= "Assets/06collagepics.jpg">

*The electronics were embedded in a semi-sphere made of mycelium in allusion to the moon (and its lunar cycles in relation to the meditation proposed for the MDFEST) made by Jasmine Boerner.

**_Iteration Process_**

<img src= "Assets/03iteration.png">
<img src= "Assets/04collagepics.jpg">

The options investigated prior to the final one:
1. _Arduino Pulse Sensor 
2. Contact Microphone 
3. ECG Graph Monitoring with AD8232 ECG Sensor & Arduino. 

**_Future development opportunities for this project_**
- Collect the data of people's heartbeat.
- Translate these data into a visual content (could be images or a different kind of sound set)
- Intermix the human heartbeat sound with the heartbeat of other living species. 

_**Link to individual page**_

https://josefina_nano.gitlab.io/mdef-website/fabacademy11.html






